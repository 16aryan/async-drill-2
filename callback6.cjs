/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/




const board = require("./boards_1.json");
const cards = require("./cards_1.json");
const list = require("./lists_1.json");
const {callback1} = require('./callback1.cjs');
const {boardInfo} = require('./callback2.cjs');
const {allCards} = require('./callback3.cjs');
const {callback5} = require('./callback5.cjs');
//callback1(board,'mcu453ed',(result)=> console.log(result));
function callback6(board,list,cards){
   callback5(board,cards,list);
   allCards("azxs123", cards, (result) => {
    console.log(result)
    allCards("cffv432", cards, (result) => {
        console.log(result)
        allCards("ghnb768", cards, (result) => { 
            console.log(result)
            allCards("isks839", cards, (result) => console.log(result));});
    });
   
   
   });
  
}

module.exports ={callback6};