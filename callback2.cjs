
/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/
const { Long } = require("mongodb");
const boards = require("./boards_1.json");
const cards = require("./cards_1.json");
const lists = require("./lists_1.json");


function boardInfo(id,boards, lists,cb){
    setTimeout(()=>{
        let count =0;
        let linkedDetails = Object.keys(lists).reduce((acc,cv)=>{
            if(cv===id){
            if(!acc[boards[count].id]){
                acc[boards[count].id]={};
            }
            if(boards[count].id== cv){
                acc[boards[count].id] =(lists[cv].flat())
      //  console.log(lists[cv]);
            }
        }
       
         count++;
         return acc;
        },{});
     
        cb(linkedDetails);
    },2000);
   
}

// Example usage:
//boardInfo('mcu453ed',boards, lists, (result) => {
  //  console.log(result);
//});
module.exports = {boardInfo};