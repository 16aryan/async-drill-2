/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/
const cards = require("./cards_1.json");
const list = require("./lists_1.json");
function allCards(id, cards, callback) {
    setTimeout(()=>{
        let result = Object.keys(cards).reduce((acc, cv) => {
            if (id === cv) {
              acc = cards[cv];
              
            }
            return acc;
          }, {});
          callback(result);
    },2000)
  
}

//allCards("qwsa221", cards, (result) => console.log(result));
module.exports = {allCards};