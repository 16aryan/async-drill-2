
/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const board = require("./boards_1.json");
const cards = require("./cards_1.json");
const list = require("./lists_1.json");
const {callback1} = require('./callback1.cjs');
const {boardInfo} = require('./callback2.cjs');
const {allCards} = require('./callback3.cjs');
//callback1(board,'mcu453ed',(result)=> console.log(result));
function callback4(board,list,cards,callback3){
    callback1(board,'mcu453ed',(result)=> {
        console.log(result);
        boardInfo('mcu453ed',board, list, (result) => {
  console.log(result);
  allCards("qwsa221", cards, (result) => console.log(result));
});
    }
    );
}

//callback4(board,list,cards)
module.exports = {callback4};